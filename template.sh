# template.sh - edit this file
NAME="Sai Suresh"

function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then 	
    echo "ERROR: $1 "
  fi
}

function display
{
    echo $NAME
    date
    echo
}



# Your script starts after this line.
if [ $# -eq 0 ]
then
   usage
else
   display
   while [ $# -gt 0 ]
   do

      case $1 in
      "TestError")
          usage "TestError found"
	  ;;
      "now") 
          echo "It is now " `date +%r`
	  ;;
      *) 
      	 usage " Do not know what to do with $1"
	 ;;
      esac 
      echo "*****"
 
      shift

   done
fi
